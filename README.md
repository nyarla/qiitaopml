QiitaOPML
=========

  * The OPML generator for the Qiita's users/tags feeds

1. Requirements
---------------

  1. Perl 5.10.0 or higher
  2. `carton`
  3. UNIX-like build environment (Linux, BSD, OSX, ...etc)

2. Usage
--------

```
# setup
$ git clone git://github.com/nyarla/qiitaopml.git ~/tmp/qiitaopml
$ cd ~/tmp/qiitaopml
$ carton install

# generate opml (for example)
$ carton exec -- perl ./qiitaopml.pl nyarla > qiita.opml
```

3. Author
---------

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

4. License
----------

  * [CC0 (pubic domain)](https://creativecommons.org/publicdomain/zero/1.0/)

5. See Also
-----------

  * [Qiita](https://qiita.com/)
