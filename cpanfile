requires 'Furl'                 => '>= 3.02';
requires 'XML::OPML::SimpleGen' => '>= 0.07';
requires 'JSON'                 => '>= 2.90';
requires 'IO::Socket::SSL'      => '>= 1.979';
requires 'URI::Escape::XS'      => '>= 0.11';
