#!/usr/bin/env perl

use 5.10.0;
use strict;
use warnings;

our $VERSION = '0.0.1';

use File::Spec;
use File::Basename qw[ dirname ];

use Furl;
use Furl::Request;

use XML::OPML::SimpleGen;
use JSON qw/ decode_json /;
use URI::Escape::XS qw/ decodeURIComponent /;

use constant {
    API_BASE_URL    => 'https://qiita.com/api/v1'
};

sub agent {
    state $furl = Furl->new(
        agent   => "QiitaOPML/${VERSION}",
        timeout => 10,
    );

    return $furl;
}

sub request {
    my ( $method, $path ) = @_;

    my $count = 0;
    my $num   = 100;
    my @ret   = ();

    while ( 1 ) {
        my $fullpath    = API_BASE_URL . $path . "?per_page=${num}&page=${count}";

        warn "Request to => ${fullpath}\n";

        my $request     = Furl::Request->new( $method => $fullpath );
        my $response    = agent->request($request);

        if ( ! $response->is_success ) {
            die "Failed to request: " . $response->message; 
        }

        my $json        = decode_json( $response->content );

        if ( ref($json) eq 'ARRAY' && scalar(@{ $json }) > 0 ) {
            push @ret, @{ $json };
            sleep(2);
        } else {
            if ( ref($json) eq 'HASH' ) {
                warn "Error: " . $json->{'error'};
            }
            last;
        }
        $count++;
    }

    return [ @ret ];
}

sub main {
    my $user    = shift @ARGV or die "Usage: ${0} [username]";
    my @users   = ();
    my $users   = request( GET => "/users/${user}/following_users" );

    unless ( ref($users) eq 'ARRAY' ) {
        die "API request is failed: " . $users->{'error'};
    } else {
        for my $usr ( @{ $users || [] } ) {
            push @users, $usr->{'url_name'};
        }
    }

    my @tags    = ();
    my $tags    = request( GET => "/users/${user}/following_tags" );

    for my $tag ( @{ $tags || [] } ) {
        push @tags, $tag->{'url_name'};
    }

    my $opml = XML::OPML::SimpleGen->new();
    
    $opml->head(
        title => 'Qiita Following User/Tag Feeds',
    );

    for my $usr ( sort @users ) {
        $opml->insert_outline(
            group   => 'Qiita Users',
            text    => "Qiita User Feed: " . decodeURIComponent($usr),
            title   => "Qiita User Feed: " . decodeURIComponent($usr),
            xmlUrl  => "http://qiita.com/${usr}/feed.atom",
        );
    }

    for my $tag ( sort @tags ) {
        $opml->insert_outline(
            group   => 'Qiita Tags',
            text    => "Qiita Tag Feed: " . decodeURIComponent($tag),
            title   => "Qiita Tag Feed: " . decodeURIComponent($tag),
            xmlUrl  => "http://qiita.com/tags/${tag}/feed.atom",
        );
    }

    print $opml->as_string;
    print "\n";

    return 0;
}

exit &main();

